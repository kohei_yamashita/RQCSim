use num::complex::Complex;

pub struct QuantumCircuit {
    num_qbits: u32,
    qbits: Vec<Complex<f64>>,
}

impl QuantumCircuit {
    pub fn new() -> Self {
        QuantumCircuit { num_qbits: 0, qbits: Vec::new()  }
    }

    pub fn add_q_bit(&mut self, n: u32) {
        for _ in 0..n.pow(2) {
            self.qbits.push(Complex::new(0.0f64, 0.0f64));
        }
        self.num_qbits = n;
    }

    pub fn set_state(&mut self, n: usize) {
        let mask: usize  = (1usize << (self.num_qbits + 1)) - 1;
        let masked_val: usize = n & mask;

        self.qbits[masked_val].re = 1.0f64;
        self.qbits[masked_val].im = 0.0f64;
    }

    pub fn show_config(&self) {
        println!("Quantum Circuit's configuration");
        println!("The num of Qbit: {:?}", self.num_qbits);
        println!("The vector of Qbit:");
        println!("{:?}", self.qbits);
    }

    // Pauli X gate: bit flip
    pub fn x(&mut self, n: usize) {
        let chk0 = 1usize << n;
        for i in 0..self.qbits.len() {
            let chk1 = i & (1usize << n);
            if  chk0 == chk1 {
                let target_idx = i & (!(1usize << n));
                let tmp = self.qbits[target_idx];
                self.qbits[target_idx] = self.qbits[i];
                self.qbits[i] = tmp;
            }
        }
    }

    // Hadamard gate: To make quantum superposition
    pub fn h(&mut self, n: usize) {
        let chk0 = 1usize << n;
        for i in 0..self.qbits.len() {
            let chk1 = i & (1usize << n);
            if  chk0 == chk1 {
                let target_idx = i & (!(1usize << n));
                let tmp = self.qbits[target_idx];
                self.qbits[target_idx] = (1.0f64 / 2.0f64.sqrt()) * (self.qbits[target_idx] + self.qbits[i]);
                self.qbits[i] = (1.0f64 / 2.0f64.sqrt()) * (tmp - self.qbits[i]);
            }
        }
    }

    // Controlled Pauli X gate
    pub fn cx(&mut self, ctr: usize, n: usize) {
        for i in 0..(self.qbits.len()/2) {
            let ctr_chk = i & (1usize << ctr);
            if ctr_chk != 0 {
                let swap_idx = i ^ (1usize << n);
                let tmp = self.qbits[swap_idx];
                self.qbits[swap_idx] = self.qbits[i];
                self.qbits[i] = tmp;
            }
        }
    }
}
