extern crate rqcsim;

fn main() {
	let mut qc = rqcsim::QuantumCircuit::new();

    qc.add_q_bit(4);
    qc.set_state(0x0);

    // Deutsch-Jozsa
    qc.h(0);
    qc.h(1);
    qc.h(2);
    qc.x(3);
    qc.h(3);
    qc.cx(0, 3);
    qc.h(0);
    qc.cx(1, 3);
    qc.h(1);
    qc.cx(2, 3);
    qc.h(2);

    qc.show_config();
}
